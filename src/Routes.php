<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 10/08/2018
 * Time: 16:51
 */

namespace Engeni\Aspects;


class Routes
{

    public static function generateRoutes()
    {

        $routes = collect([]);
        foreach(app()->routes->getRoutes() as $route)
        {
            if(array_key_exists('as', $route->action) && strpos($route->action['as'], '.index')) {
                $controller = str_replace($route->action['namespace'] . '\\', '', $route->action['controller']);
                $controller = str_replace('@index', '', $controller);
                $url = str_replace('api/', '', $route->uri);
                $url = str_replace('web/', '', $url);
                $url = $url . '/builder/aspects';
                $controller = $controller . '@aspects';
                $routes = $routes->push(['url' => $url, 'controller_action' => $controller]);
            }
        }
        return $routes;
    }
    
}