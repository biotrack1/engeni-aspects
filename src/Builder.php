<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\Aspects;

use Engeni\Aspects\Traits\Helper;
use Illuminate\Support\Facades\DB;

class Builder
{
    public $model_class;
    public $aspects_class;
    public $aspects_table;
    public $index_aspects;
    public $allowed_columns;
    public $ignored_columns;
    public $search_fields;
    public $associations;
    protected $dateColumns;
    protected $table_reflections;
    protected $_table_columns;
    protected $table_name;
    protected $table_columns;
    protected $fillable;
    protected $display_string;
    static $schemaInformationCache = [];
    static $alreadyBuildAspects = false;
    static $builderCache = [];
    static $datesCache = [];

    use Helper;

    # @param [Class] model_class to which aspect belongs
    # @param [Class] aspects_class type of aspect
    # @return [AspectsBuilder]
    public function __construct($model_class, $aspectsClass = Aspect::class)
    {
        $this->model_class = $model_class;
        $this->aspects_class = $aspectsClass;
        $this->aspects_table = collect([]);
        $this->index_aspects = collect([]);
        $this->allowed_columns = $this->defaultAllowedColumns();
        $this->ignored_columns = $this->defaultIgnoredColumns();
        $this->search_fields = collect([]);
        $this->buildAspects();
    }

    private function setSchemaInformation()
    {
        $this->associations = $this->getAssociations();
        $this->table_name = $this->getTableName();
        $this->table_columns = $this->getTableColumns();
        $this->_table_columns = $this->table_columns->toArray();
        $this->table_reflections = $this->getTableReflections();
    }

    public function getFillable()
    {
        return $this->fillable;
    }

    public function setFillable()
    {
        if ($this->isEloquentModel()) {
            $fillable = $this->formAspects()->map(function ($aspect) {
                return $aspect->strongParam();
            })->values()->toArray();

            $this->fillable = $fillable;
        }
    }

    public function tableColumns()
    {
        return $this->table_columns;
    }

    public function associations()
    {
        return $this->associations;
    }

    public function setAssociationOptions($associations, $options)
    {
        foreach ($associations as $association) {
            if ($this->associations()->has($association)) {
                $asc = $this->associations()[$association];
                $asc->setoptions($options);
            }
        }
    }

    private function getTableColumns()
    {
        if (empty(static::$schemaInformationCache[$this->table_name])) {
            static::$schemaInformationCache[$this->table_name] = collect($this->modelInstance()->getConnection()->getSchemaBuilder()->getColumnListing($this->table_name));
        }
        return static::$schemaInformationCache[$this->table_name];
    }

    public function getTableColumnsArray()
    {
        return $this->_table_columns;
    }

    public function tableName()
    {
        return $this->table_name;
    }

    # @return [Array] columns used as metadata to build the receiver.
    # An empty return value means simply that ALL are allowed.
    public function defaultAllowedColumns()
    {
        return collect([]);
    }

    # @return [Array] columns IGNORED to build the receiver.
    public function defaultIgnoredColumns()
    {
        return collect(['id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted_by', 'deleted_at']);
    }

    private function includeColumn($column)
    {
        if ($this->allowed_columns->count() > 0) {
            return $this->allowed_columns->contains($column);
        }
        return !$this->ignored_columns->contains($column);
    }

    # @return [Integer] the default number of columns in forms
    public function defaultFormColumns()
    {
        return 2;
    }

    # Useful for including modules like AspectsDSL
    public function builder()
    {
        return $this;
    }

    # Return the aspects in the same order they were added
    # @return [Array<Aspects::Aspect>]
    public function aspects()
    {
        return $this->aspects_table;
    }

    # Return the aspects sorted by priority
    # @return [Array<Aspects::Aspect>]
    public function sortedAspects()
    {
        return $this->aspects()->sortBy('options.priority');
    }

    # Return the aspects of the receiver used in index alike views
    # @return [Array<Aspects::Aspect>]
    public function indexAspects()
    {
        if ($this->index_aspects->isEmpty()) {
            return $this->previewAspects();
        }
        return $this->index_aspects()->map(function ($aspectKey) {
            return $this->aspects()->get($aspectKey);
        });
    }

    public function previewAspects()
    {
        return $this->sortedAspects()->filter(function ($aspect) {
            return $aspect->isVisible();
        });
    }

    # Return an array containing the requested aspects
    # @param [Array] names_list
    # @return [Array<Aspect>]
    public function mapAspects($namesArray)
    {
        $aspects = collect([]);
        foreach ($namesArray as $aspectName) {
            if ($this->aspects_table->has($aspectName)) {
                $aspects->push($this->aspects_table->get($aspectName));
            }
        }
        return $aspects;
    }

    # Set the index aspects of the receiver
    # @param [Array<Symbol>] aspects to set
    public function setIndexAspects($aspects)
    {
        $this->index_aspects = $aspects;
    }

    # Return the aspects of the receiver used in forms
    # @return [Array<Aspects::Aspect>]
    public function formAspects()
    {
        return $this->sortedAspects()->filter(function ($aspect) {
            return $aspect->isEditable();
        });
    }

    # Return fillable attributes for model or controller uses
    # @return [Array]
    public function fillableAttributes()
    {
        return $this->formAspects()->map(function ($aspect) {
            return $aspect->strongParam();
        })->values()->toArray();
    }

    # Return the importable aspects of the receiver
    # @return [Array<Aspects::Aspect>]
    public function importableAspects()
    {
        return $this->sortedAspects()->filter(function ($aspect) {
            return $aspect->isImportable();
        });
    }

    # Return the identifier aspects of the receiver
    # @return [Array<Aspects::Aspect>]
    public function identifierAspects()
    {
        return $this->sortedAspects()->filter(function ($aspect) {
            return $aspect->isIdentifier();
        });
    }

    # Return hidden aspects used in forms
    # @return [Array<Aspects::Aspect>]
    public function hiddenFormAspects()
    {
        $this->formAspects()->filter(function ($aspect) {
            return $aspect->isHidden();
        });
    }

    # Return the csv exportable aspects of the receiver
    # @return [Array<Aspects::Aspect>]
    public function csvAspects()
    {
        return $this->previewAspects();
    }

    # Set options for the receiver's aspects
    # @param [Array | Symbol | String] aspects to configure with options
    # @param [Array] options to be applied to each of the aspects
    public function setOptions($aspects, $options)
    {
        foreach ($aspects as $aspect) {
            if ($this->aspects()->has($aspect)) {
                $asp = $this->aspects()[$aspect];
                $asp->setoptions($options);
            }
        }
    }

    public function addNewAspect($aspectName, $options = [])
    {
        $options = collect($options);

        if ($this->aspects()->has($aspectName) && !$options->get('override')) # Add only if does not exists
        {
            return $this->aspects()->get($aspectName);
        }
        $aspect = new $this->aspects_class($this->aspects_class,
            $aspectName, $options->get('accessor', $aspectName),
            $options->get('type'),
            $options->get('default'), $options->get('null'), $options->toArray());
        $this->setAspect($aspectName, $aspect);
        return $aspect;
    }

    # Set the aspects used in search
    # @param [Array] fields where keys are aspects and values are conditions
    public function setSearchFields($fields)
    {
        $searchFields = collect([]);
        foreach ($fields as $asp => $condition) {
            if ($this->aspects()->has($asp)) {
                if (!is_array($condition)) {
                    $searchFields->put($asp, "{$asp}_{$condition}");
                } else {
                    $conditions = collect($condition)->map(function ($cond) use ($asp) {
                        return "{$asp}_{$cond}";
                    });
                    $searchFields->put($asp, $conditions);
                }
            }
        }
        $this->search_fields = $searchFields;
    }

    # Return the search fields of the receiver
    # @return [Array] fields where keys are aspects and values are conditions
    public function searchFields()
    {
        return $this->search_fields;
    }

    # Return the aspects of the receiver used in tabbed forms
    # @return [Array<Aspects::Aspect>]
    public function tabbedAspects()
    {
        $tabbed = $this->formAspects()->reject(function ($asp, $key) {
            return $asp->isHidden();
        });
        $tabbed = $tabbed->groupBy(function ($asp, $key) {
            return $asp->tab();
        });
        return $tabbed;
    }

    ### PRIVATE METHODS ###

    private function getDoctrineSchemaManager()
    {
        return $this->modelInstance()->getConnection()->getDoctrineSchemaManager();
    }

    private function registerAnothersTypeMappings()
    {
        $platform = $this->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
    }

    private function buildAspects()
    {
        self::$alreadyBuildAspects = true;
        $this->buildFromEloquent();
        $this->customizeAspects();
        $this->setValidations();
        $this->setFillable();
        $this->removeInstanceFromModelClassAttribute();
        self::$alreadyBuildAspects = false;
    }

    private function removeInstanceFromModelClassAttribute()
    {
        $this->model_class = get_class($this->model_class);
    }

    # This is the #customize_aspects hook for customization
    public function customizeAspects()
    {
        $this->setAllowedColumns();
        $this->setIndexAspects($this->aspects()->take(8)->keys());
    }

    public function setIdAspect()
    {
        if($this->contentColumns()->contains('id'))
        {
            $this->addNewAspect('id', [
                'type' => 'integer', 'editable' => false, 'importable' => false, 'identifier' => true, 'visible' => true, 'priority' => 0]);
        }
    }

    public function aspectOptions($aspects, $options)
    {
        $this->setOptions((array)$aspects, $options);
    }

    public function setAllowedColumns()
    {
        $this->allowed_columns = $this->aspects()->keys()->diff($this->defaultIgnoredColumns());
    }

    # Defined as an eloquent if responds to the set of methods upon the receiver depends.
    private function isEloquentModel()
    {
        return $this->isSubClassOf($this->model_class, $this->eloquentModel());
    }

    private function buildFromEloquent()
    {
        if ($this->isEloquentModel()) {
            $this->registerAnothersTypeMappings();
            $this->setSchemaInformation();
            $this->buildFromContentColumns();
            $this->buildFromAssociationColumns();
            $this->applyDefaultPriority();
            $this->setDateColumns();
            $this->setIdAspect();
        }
    }

    private function setDateColumns()
    {
        $this->dateColumns = $this->aspects()->filter(function ($asp) {
            return $asp->type == 'datetime';
        })->map(function ($asp) {
            return $asp->strongParam();
        })->values()->toArray();

    }

    public function getDates()
    {
        return $this->dateColumns;
    }

    private function buildFromContentColumns()
    {
        foreach ($this->contentColumns() as $column) {
            if ($this->includeColumn($column)) {
                $name = $column;
                $aspect = $this->contentColumnAspect($column);
                $this->setAspect($name, $aspect);
            }
        }
    }

    private function belongsToAssociations()
    {
        return $this->associations->filter(function ($ass) {
            return $ass->type == 'BelongsTo';
        });
    }

    private function buildFromAssociationColumns()
    {
        foreach ($this->belongsToAssociations() as $association) {
            if ($this->includeColumn($association->foreignKey)) {
                $name = snake_case($association->name);
                $aspect = $this->associationColumnAspect($association);
                $this->setAspect($name, $aspect);
            }
        }
    }

    private function contentColumnAspect($column)
    {
        return $this->getAspectForEloquentColumn($column);
    }

    private function associationColumnAspect($association)
    {
        return $this->getAspectForEloquentAssociationColumn($association);
    }

    private function getAspectForEloquentColumn($column)
    {
        $defClass = get_class($this->model_class);
        $name = $column;
        $accessor = $column;
        $type = $this->getColumnDataType($column);
        $default = $this->getColumnDefault($column);
        $null = !$this->getColumnNotNull($column);
        $options = ['editable' => true, 'importable' => true];
        return new $this->aspects_class($defClass, $name, $accessor, $type, $default, $null, $options);
    }

    private function setValidations()
    {
        if ($this->isEloquentModel()) {
            $validations = collect([]);
            $aspects = $this->formAspects();
            foreach ($aspects as $aspect) {
                $column = $aspect->strongParam();
                if ($this->existsColumnReflection($column)) {
                    $columnValidation = new Validation($this, $column, $aspect);
                    $validations->put($column, $columnValidation->toArray());
                }
            }
            $this->validations = $validations->merge($this->modelExplicitRules());
        }
    }

    private function existsColumnReflection($column)
    {
        return array_key_exists($column, $this->getColumnReflections());
    }

    private function modelExplicitRules()
    {
        return \Illuminate\Support\Facades\Validator::make([], $this->model_class->getImplicitRules())->getRules();
    }

    # Construct only BelongsTo Associations.
    private function getAspectForEloquentAssociationColumn($association)
    {
        $defClass = get_class($this->model_class);
        $name = snake_case($association->name);
        $accessor = $association->name;
        $type = $association->type;
        $default = null;
        $null = !$this->getColumnNotNull($association->foreignKey);
        $options =
            [
                'foreign_key' => $association->foreignKey,
                'owner_key' => $association->ownerKey,
                'association_class' => $association->model,
                'editable' => true,
                'visible' => true,
                'importable' => true
            ];

        return new $this->aspects_class($defClass, $name, $accessor, $type, $default, $null, $options);
    }

    private function setAspect($aspectName, $aspectObject)
    {
        $this->aspects_table->put($aspectName, $aspectObject);
    }

    private function contentColumns()
    {
        return $this->table_columns->diff($this->associationColumns());
    }

    private function associationColumns()
    {
        return $this->belongsToAssociations()->pluck('foreignKey');
    }

    private function getAssociations()
    {
        return (new Relationships($this->modelInstance()))->all();
    }

    public function getTableName()
    {
        return $this->modelInstance()->getTable();
    }

    private function modelInstance()
    {
        return $this->model_class;
    }

    private function getTableReflections()
    {
        return $this->getDoctrineSchemaManager()->listTableDetails($this->table_name);
    }

    public function getColumnReflections()
    {
        return $this->table_reflections->getColumns();
    }

    public function getIndexes()
    {
        return $this->table_reflections->getIndexes();
    }

    private function getColumnDataType($column)
    {
        return $this->getColumnReflections()[$column]->getType()->getName();
    }

    private function getColumnDefault($column)
    {
        return $this->getColumnReflections()[$column]->getDefault();
    }

    private function getColumnNotNull($column)
    {
        return $this->getColumnReflections()[$column]->getNotNull();
    }

    private function applyDefaultPriority()
    {
        $idx = 1;
        foreach ($this->aspects() as $aspect) {
            $aspect->setPriority($idx);
            $idx = $idx + 1;
        }
    }

    public function displayString()
    {
        return $this->display_string;
    }

}