<?php
/**
 * Created by PhpStorm.
 * User: mcalvo
 * Date: 09/08/2018
 * Time: 16:13
 */

namespace Engeni\Aspects\Converters;

class StringToText extends ObjectToText
{
    public function displayFor($object)
    {
        return $object ? $object : $this->nullString();
    }
}